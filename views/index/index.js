$(document).ready(function(){
	var socket = io({ path: "/socket.io/", transports: ["websocket"] }), tempDots = null, dots = new Array();

	socket.on("newDots", function(newDots){
		for(var i = 0; i < newDots.length; i++)
			if(!$("div#" + newDots[i].x + '_' + newDots[i].y).length)
				$("body").append("<div id='" + newDots[i].x + '_' + newDots[i].y + "' class='dot' style='left: " + newDots[i].x + "px; top: " + newDots[i].y + "px; background-color: " + newDots[i].color + "'></div>");
			else
				$("div#" + newDots[i].x + '_' + newDots[i].y).style("background-color", newDots[i].color)
	});
	socket.on("destroyAll", function(){
		$("div").remove();
	});


	$.getJSON("/dots.json", function(data){
		dots = dots.concat(data);

		for(var i = 0; i < dots.length; i++)
			$("body").append("<div id='" + dots[i].x + '_' + dots[i].y + "' class='dot' style='left: " + dots[i].x + "px; top: " + dots[i].y + "px; background-color: " + dots[i].color + "'></div>");
	});

	var isLeft = false;
	$("body").on("mousedown touchstart", function(e){
		if(!e.which || e.which == 1){
			isLeft = true;
			tempDots = new Array();
			$("body").on("mousemove touchmove", function(e){
				tempDots.push({ x: e.pageX || e.originalEvent.touches[0].pageX, y: e.pageY || e.originalEvent.touches[0].pageY });

				$("body").append("<div class='dot temp' style='left: " + (e.pageX || e.originalEvent.touches[0].pageX) + "px; top: " + (e.pageY || e.originalEvent.touches[0].pageY) + "px'></div>");
			});
		}
	});

	$("body").on("mouseup touchend", function(e){
		$("body").unbind("mousemove");
		$("body").unbind("touchmove");

		if(isLeft)
			$.ajax({
				url : "/dots.json",
				type: "post",
				data : { events: tempDots && tempDots.length ? tempDots : [{ x: e.pageX || e.originalEvent.touches[0].pageX, y: e.pageY || e.originalEvent.touches[0].pageY }] },
				success: function(data, textStatus, jqXHR){
					$("div.temp").remove();
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert("Ups...");
				}
			});
	});

	$("body").on("keydown", function(e){
		if(e.keyCode == 68)
			$.ajax({
				url : "/dots.json",
				type: "delete",
				data : {}
			});
	});
});
