exports.dot = Waterline.Collection.extend({
	identity: 'dot',
	tableName: 'coordinates',
	meta: {
		schemaName: 'public'
	},
	connection: 'pgConn',
	attributes: {
		x: {
			type: 'float',
			required: true
		},
		y: {
			type: 'float',
			required: true
		},
		color: {
			type: 'string'
		}
	}
});
