var postgresAdapter = require('sails-postgresql');

GLOBAL.config = {
	adapters: {
		postgres: postgresAdapter
	},
	connections: {
		pgConn: {
			database: 'Seedrs',
			host: 'localhost',
			user: 'postgres',
			password: 'postgres',
			port: 5432,
			poolSize: 10,
			ssl: false,
			adapter: "postgres"
		}
	}
};
