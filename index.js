Object.defineProperty(global, "__stack", {
	get: function(){
		var orig = Error.prepareStackTrace;
		Error.prepareStackTrace = function(_, stack) {
			return stack;
		};
		var err = new Error;
		Error.captureStackTrace(err, arguments.callee);
		var stack = err.stack;
		Error.prepareStackTrace = orig;
		return stack;
	}
});
Object.defineProperty(global, "__line", {
	get: function(){
		return __stack[1].getLineNumber();
	}
});
Object.defineProperty(global, "__function", {
	get: function(){
		return __stack[1].getFunctionName();
	}
});

require("./confs.js");

var fs = require("fs");
var pg = require("pg");
var async = require("async");
var Color = require("color");
GLOBAL.Waterline = require('waterline');

var bodyParser = require("body-parser");

var express = require("express");
var app = express();
var http = require("http");

http.globalAgent.maxSockets = Infinity;
http = http.Server(app);

var redis = require("socket.io-redis");
GLOBAL.io = require("socket.io")(http);
io.adapter(redis({ host: "localhost", port: 6379 }));
io.on("connection", function(socket){
	socket.join("events");
});

app.use(bodyParser.json({ limit: "150mb" }));
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use('/public', express.static(__dirname + '/public'));
app.use('/views', express.static(__dirname + '/views'));

app.get("/", function(request, response){
	response.writeHead(200, { "Content-Type": "text/html" });
	response.end(fs.readFileSync(__dirname + "/views/index/index.html"));
});
app.get("/dots.:format(json)", function(request, response){
	app.models.dot.find().exec(function(error, dots){
		if(error){
			response.writeHead(403, { "Content-Type": "text/plain" });
			response.end("ErrorExecuting");
		}else{
			response.writeHead(200, { "Content-Type": "application/json" });
			response.end(new Buffer(JSON.stringify(dots)));
		}
	});
});

app.post("/dots.:format(json)", function(request, response){
	var asyncTasks = new Array();
	request.body.events.forEach(function(item, index){
		asyncTasks.push(function(callback){
			app.models.dot.find().where({x: item.x, y: item.y}).exec(function(error, dots){
				if(error)
					callback(error);
				else{
					item.color = Color().rgb(255, 107, 78).rotate(new Date().getTime() % 1000 / 3).hexString();

					if(!dots.length)
						app.models.dot.create(item, function(error, dot){
							callback(error);
						});
					else
						app.models.dot.update({x: item.x, y: item.y}, item, function(error, dot){
							callback(error);
						});
				}
			});
		});
	});
	async.parallel(asyncTasks, function(error){
		if(error){
			response.writeHead(403, { "Content-Type": "text/plain" });
			response.end("ErrorExecuting");
		}else{
			io.to("events").emit("newDots", request.body.events);

			response.writeHead(200, { "Content-Type": "text/plain" });
			response.end("Done");
		}
	});
});

app.delete("/dots.:format(json)", function(request, response){
	app.models.dot.destroy().exec(function(error, dots){
		if(error){
			response.writeHead(403, { "Content-Type": "text/plain" });
			response.end("ErrorExecuting");
		}else{
			io.to("events").emit("destroyAll");

			response.writeHead(200, { "Content-Type": "text/plain" });
			response.end("Done");
		}
	});
});

var orm = new Waterline();
orm.loadCollection(require("./lib/dot.js").dot);
orm.initialize(config, function(error, models) {
	if(error) throw error;

	app.models = models.collections;
	app.connections = models.connections;

	http.listen(3000);
});
